# Reentry Simulation using S2 S2 Protocol

This project generates a reentry using the S2 S2 protocol, saves it, and then reruns it for 300 ms. The simulations are performed on the same geometry with different resolutions. 

First clone the repository with the examples:  

```bash
git clone https://gitlab.kit.edu/kit/ibt-public/openCARP_DREAM_Examples.git
```
Then, change the directory to run the examples from the folder containing the repository.

```bash
cd /path/to/openCARP_DREAM_Examples
```

Depending on whether you want to compile from the source (Option 1) or use a docker image (Option 2), follow the instructions below.


## Option 1- Compiling from source


### Prerequisites
Ensure you have the necessary dependencies installed. This script depends on the following modules:
- `os`
- `math`
- `shutil`
- `time`
- `carputils`
- `carpio`


Clone and compile openCARP from the openCARP_DREAM  branch in the openCARP repository(https://git.opencarp.org/openCARP/openCARP/-/tree/openCARP_DREAM)
Make sure you install meshalyzer to visualize the results (https://git.opencarp.org/openCARP/meshalyzer)



### Running the Simulation

You can run the simulation with different configurations by passing arguments to the script. Below are the parameters you can specify:

- `--model`: Ionic model to use (1=Courtemanche, 2=Bueno Orovio). Default is 1.
- `--af_status`: AF status (1=chronic AF, 0=Healthy). Default is 1.
- `--geom`: Geometry used (0=slab, 1=realistic). Default is 0.
- `--resol`: Mesh resolution in micrometers. Default is 400. options: 200, 400, 800 and 1600.
- `--velocity`: Conduction velocity in the mesh in mm/s. Default is 200.
- `--ar`: Anisotropic ratio. Default is 4.
- `--timeS2`: Time for the second stimulus in ms. Default is 100.
- `--eik_sol`: Solver to use (0=monodomain, 1=DREAM). Default is 0.

### Example Usage

To run a DREAM simulation with  800 micrometers mesh resolution:

```bash
python reentry.py --eik_sol 1 --resol 800
```

To run a monodomain simulation with a 400 micrometers mesh resolution:
```bash
python reentry.py --resol 400
```
To visualize in meshalizer add the parameter --visualize with no input

```bash
python reentry.py --resol 400 --visualize
```

Warning:
If you try to run the monodomain model with resolutions of 800 or 1600 micrometers there will be an error due to propagation failure

If you run in terminal all the possible experiments and the post processing script it will analize the computing times
```bash
# Monodomain simulations
python reentry.py --resol 200 --overwrite-behaviour overwrite &
python reentry.py --resol 400  --overwrite-behaviour overwrite &
# DREAM simulations
python reentry.py --eik_sol 1 --resol 200 --overwrite-behaviour overwrite &
python reentry.py --eik_sol 1 --resol 400 --overwrite-behaviour overwrite &
python reentry.py --eik_sol 1 --resol 800 --overwrite-behaviour overwrite &
python reentry.py --eik_sol 1 --resol 1600 --overwrite-behaviour overwrite &
python Postprocessing.py
```

## Option 2- Docker Image

### Prerequisites:

- docker
- meshalyzer, not necessary for running the example, but for visualizing results
  (https://git.opencarp.org/openCARP/meshalyzer)

To pull the docker image run this command 

```bash
docker pull docker.opencarp.org/opencarp/opencarp:openCARP_DREAM
```
### Running the Simulation

You can run the simulation with different configurations by passing arguments to the script. Below are the parameters you can specify:

- `--model`: Ionic model to use (1=Courtemanche, 2=Bueno Orovio). Default is 1.
- `--af_status`: AF status (1=chronic AF, 0=Healthy). Default is 1.
- `--geom`: Geometry used (0=slab, 1=realistic). Default is 0.
- `--resol`: Mesh resolution in micrometers. Default is 400. options: 200, 400, 800 and 1600.
- `--velocity`: Conduction velocity in the mesh in mm/s. Default is 200.
- `--ar`: Anisotropic ratio. Default is 4.
- `--timeS2`: Time for the second stimulus in ms. Default is 100.
- `--eik_sol`: Solver to use (0=monodomain, 1=DREAM). Default is 0.

###  Example Usage

To run a DREAM simulation with  1600 micrometers mesh resolution:

```bash
docker run -v $PWD:/example -w /example \
--entrypoint "python" \
docker.opencarp.org/opencarp/opencarp:openCARP_DREAM \
reentry.py --resol 1600 --eik_sol 1 --overwrite-behaviour overwrite
```
To run a monodomain simulation with  400 micrometers mesh resolution:

```bash
docker run -v $PWD:/example -w /example \
--entrypoint "python" \
docker.opencarp.org/opencarp/opencarp:openCARP_DREAM \
reentry.py --resol 400  --overwrite-behaviour overwrite
```

You can visualize the results with meshalyzer:

```bash
meshalyzer Meshes/1600um/Slab1600 Reentry/Computertimes_CAS16D/vm.igb
```
