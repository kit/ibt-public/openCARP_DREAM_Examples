import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mpl

# Load data
Times_CAS02D = np.loadtxt('Reentry/Computertimes_CAS02D/Time_eikonal.dat')
Times_CAS04D = np.loadtxt('Reentry/Computertimes_CAS04D/Time_eikonal.dat')
Times_CAS08D = np.loadtxt('Reentry/Computertimes_CAS08D/Time_eikonal.dat')
Times_CAS16D = np.loadtxt('Reentry/Computertimes_CAS16D/Time_eikonal.dat')
Times_CAS02M = np.loadtxt('Reentry/Computertimes_CAS02M/Time_eikonal.dat')
Times_CAS04M = np.loadtxt('Reentry/Computertimes_CAS04M/Time_eikonal.dat')
Times_CAS08M = np.loadtxt('Reentry/Computertimes_CAS08M/Time_eikonal.dat')
Times_CAS16M = np.loadtxt('Reentry/Computertimes_CAS16M/Time_eikonal.dat')

Times_CAS02D_RD = np.loadtxt('Reentry/Computertimes_CAS02D/Time_RD.dat')
Times_CAS04D_RD = np.loadtxt('Reentry/Computertimes_CAS04D/Time_RD.dat')
Times_CAS08D_RD = np.loadtxt('Reentry/Computertimes_CAS08D/Time_RD.dat')
Times_CAS16D_RD = np.loadtxt('Reentry/Computertimes_CAS16D/Time_RD.dat')
Times_CAS02M_RD = np.loadtxt('Reentry/Computertimes_CAS02M/Time_RD.dat')
Times_CAS04M_RD = np.loadtxt('Reentry/Computertimes_CAS04M/Time_RD.dat')
Times_CAS08M_RD = np.loadtxt('Reentry/Computertimes_CAS08M/Time_RD.dat')
Times_CAS16M_RD = np.loadtxt('Reentry/Computertimes_CAS16M/Time_RD.dat')

# Create DataFrame for Time_RD
row_names_rd = [
    'Initialise Ionic',
    'Compute Step Ionic',
    'Output Step Ionic',
    'Initialise Electrics',
    'Compute Steps Electrics',
    'Output Step Electrics'
]

df_rd = pd.DataFrame({
    '02D': Times_CAS02D_RD,
    '04D': Times_CAS04D_RD,
    '08D': Times_CAS08D_RD,
    '16D': Times_CAS16D_RD,
    '02M': Times_CAS02M_RD,
    '04M': Times_CAS04M_RD,
    '08M': Times_CAS08M_RD,
    '16M': Times_CAS16M_RD
}, index=row_names_rd)
df_rd = df_rd.round(1)

# Create DataFrame for Reentry times
row_names = [
    'Initialization',
    'ApplyStimuli',
    'Step D',
    'Step A',
    'Step B',
    'Coherence',
    'Solving Edges',
    'Solving Triangles',
    'Solving Tetrahedra',
    'Total Time Eikonal',
    'Total Simulation'
]

df_reentry = pd.DataFrame({
    '02D': Times_CAS02D,
    '04D': Times_CAS04D,
    '08D': Times_CAS08D,
    '16D': Times_CAS16D,
    '02M': Times_CAS02M,
    '04M': Times_CAS04M,
    '08M': Times_CAS08M,
    '16M': Times_CAS16M
}, index=row_names)
df_reentry = df_reentry.round(1)
df_reentry = pd.concat([df_reentry, df_rd])
print('Reentry')
print(df_reentry)

df_reentry.to_excel('reentry_times_data.xlsx')

# Extract total times
Total_Times_CAS02D = Times_CAS02D[-1] # Model DREAM at 200 um resolutions 
Total_Times_CAS04D = Times_CAS04D[-1] # Model DREAM at 400 um resolutions
Total_Times_CAS08D = Times_CAS08D[-1] # Model DREAM at 800 um resolutions
Total_Times_CAS16D = Times_CAS16D[-1] # Model DREAM at 1600 um resolutions
Total_Times_CAS02M = Times_CAS02M[-1] # Model Monodomain at 200 um resolutions
Total_Times_CAS04M = Times_CAS04M[-1] # Model Monodomain at 400 um resolutions
Total_Times_CAS08M = 0 # Model Monodomain at 800 um resolutions
Total_Times_CAS16M = 0 # Model Monodomain at 1600 um resolutions

# Total times for DREAM model
total_times_dream = [Total_Times_CAS02D, Total_Times_CAS04D, Total_Times_CAS08D, Total_Times_CAS16D]

# Total times for Monodomain model
total_times_monodomain = [Total_Times_CAS02M, Total_Times_CAS04M, Total_Times_CAS08M, Total_Times_CAS16M]

# Plotting parameters
width = 8
Fontsize_val = 26
Fontsize_val_leg = Fontsize_val
Fontsize_val_tics = Fontsize_val
mpl.rcParams['font.family'] = 'serif'
bar_width = 0.35
resolutions = ['200', '400', '800', '1600']

# Bar positions for Monodomain and DREAM models
bar_positions_monodomain = np.arange(len(resolutions))
bar_positions_dream = bar_positions_monodomain + bar_width

# Plot
fig, axs = plt.subplots(1, 1, figsize=(2 * width, width))

# Create bar plots
axs.bar(bar_positions_dream, total_times_dream, width=bar_width, label='DREAM')
axs.bar(bar_positions_monodomain, total_times_monodomain, width=bar_width, label='Monodomain')

# Set axis labels and title
axs.set_xlabel('Resolution in μm', fontsize=Fontsize_val)
axs.set_ylabel('Computational times in s', fontsize=Fontsize_val)
axs.tick_params(axis='both', which='major', labelsize=Fontsize_val_tics)
axs.set_xticks(bar_positions_monodomain + bar_width / 2)
axs.set_xticklabels(resolutions, fontsize=Fontsize_val_tics)

# Add legend
axs.legend(fontsize=Fontsize_val_leg, prop={'family': 'serif', 'weight': 'normal', 'size': Fontsize_val_leg}, markerscale=0.8, labelspacing=0.8, loc='upper right')

# Adjust layout
plt.tight_layout()

# Save the plot
plt.savefig('Figures/ComputationalTimes_PlanarWave_Reentry.pdf', bbox_inches="tight")

# Show the plot
plt.show()
