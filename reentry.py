import os
import math
import shutil
import time
from os import path

from carputils import settings, tools, mesh, carpio
from carputils.carpio import txt

EXAMPLE_DESCRIPTIVE_NAME = 'Experiments homogeneous'
EXAMPLE_DIR = os.path.dirname(__file__)

# Define parameters exposed to the user on the command line
def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')
    group.add_argument('--model', type=int, default=1, help='ionic model 1=Courtemanche 2=Bueno Orovio')
    group.add_argument('--af_status', type=int, default=1, help='AF status 1=chronic AF 0=Healthy')
    group.add_argument('--geom', type=int, default=0, help='Geometry used 0=slab 1=realistic')
    group.add_argument('--resol', type=int, default=400, help='Mesh resolution in um')
    group.add_argument('--velocity', type=float, default=200, help='CV in all mesh mm/s')
    group.add_argument('--ar', type=float, default=4, help='Anisotropic ratio')
    group.add_argument('--timeS2', type=int, default=100, help='Time for second stimulus')
    group.add_argument('--eik_sol', type=int, default=0, help='If 0 monodomain is used, if 1 DREAM is used')
    return parser

def jobID(args):
    """Generate name of top-level output directory."""
    model = {1: 'C', 2: 'B', 3: 'M'}.get(args.model, 'C')
    status = 'A' if args.af_status == 1 else 'H'
    geom = 'S' if args.geom == 0 else 'R'
    dream = 'M' if args.eik_sol == 0 else 'D'
    res = {200: '02', 400: '04', 800: '08', 1600: '16'}.get(args.resol, '04')

    filename = f'Computertimes_{model}{status}{geom}{res}{dream}'
    return os.path.join('Reentry', filename)

def single_cell_initialization(job, steady_state_dir, bcl, modelpar):
    if not path.exists(steady_state_dir):
        os.mkdir(steady_state_dir)
    
    num_stim_cell = 51
    duration = num_stim_cell * bcl
    init_file = os.path.join(steady_state_dir, f'init_values_{bcl}.sv')
    
    cmd = [
        settings.execs.BENCH,
        '--imp', "Courtemanche",
        '--imp-par', modelpar,
        '--bcl', bcl,
        '--dt-out', 1,
        '--stim-curr', 20,
        '--stim-dur', 2,
        '--numstim', num_stim_cell,
        '--duration', duration,
        '--stim-start', 0,
        '--dt', 20 / 1000,
        '--fout', os.path.join(steady_state_dir, 'output'),
        '-S', duration,
        '-F', init_file
    ]

    job.bash(cmd)
    tissue_init = [f'-imp_region[0].im_sv_init', init_file]
    return tissue_init

def execute(args, job, resolution=None, converge=False, lumping=None, ts=None, model=None, gi=None, ge=None, modelpar=None, cvfile='results.dat', velocity=None):
    if os.path.isfile(cvfile):
        os.remove(cvfile)

    current_directory = os.getcwd()
    tuneCV_directory = os.path.join(current_directory, f'tuneCV__{resolution}_{model}_{modelpar}')
    if not path.exists(tuneCV_directory):
        os.mkdir(tuneCV_directory)
    os.chdir(tuneCV_directory)

    cmd = [
        settings.execs.TUNECV,
        '--resolution', args.resol,
        '--modelpar', modelpar,
        '--tol', 0.010,
        '--lumping', False,
        '--velocity', velocity / 1000,
        '--converge', True,
        '--length', 2,
        '--stimS', 800,
        '--model', model,
        '--maxit', 50
    ]

    job.bash(cmd)
    
    try:
        mcv = txt.read(cvfile)
    except:
        print('Could not read cvfile')
        return -1

    os.chdir(current_directory)
    shutil.rmtree(tuneCV_directory)

    if args.ar > 0.:
        return mcv[1], mcv[2]
    else:
        return mcv[0]

@tools.carpexample(parser, jobID)
def prepace(args, job):
    current_directory = os.getcwd()
    meshname = os.path.join(current_directory, 'Meshes', f'{args.resol}um', f'Slab{args.resol}')
    cmd = tools.carp_cmd(tools.simfile_path(os.path.join(EXAMPLE_DIR, 'simple.par')))
    
    p_cvrest = 100000 if args.af_status == 1 else 811
    k_cvrest = 116.40863888 if args.af_status == 1 else 53
    j_cvrest = 66.278217 if args.af_status == 1 else 137

    imp_model = 'Courtemanche'
    k = 0 if args.af_status == 1 else 1
    g_K1 = [2, 1]
    g_Na = [1, 1]
    blf_i_Kur = [0.5, 1]
    g_to = [0.35, 1]
    g_Ks = [2, 1]
    maxI_pCa = [1.5, 1]
    maxI_NaCa = [1.6, 1]
    g_Kr = [1, 1]
    g_CaL = [0.45, 1]

    modelpar = f'GCaL*{g_CaL[k]},GK1*{g_K1[k]},GNa*{g_Na[k]},factorGKur*{blf_i_Kur[k]},Gto*{g_to[k]},GKs*{g_Ks[k]},maxIpCa*{maxI_pCa[k]},maxINaCa*{maxI_NaCa[k]}'
    steady_state_dir = 'single_cell_initialization'
    tissueinit = single_cell_initialization(job, steady_state_dir, 250, modelpar)
    
    ATs = [
        '-num_LATs', 2,
        '-lats[0].ID', 'AT',
        '-lats[1].ID', 'RT',
        '-lats[0].mode', 0,
        '-lats[0].threshold', -35,
        '-lats[0].all', 1,
        '-lats[1].mode', 1,
        '-lats[1].threshold', -40,
        '-lats[1].all', 1
    ]

    stim = [
        '-num_stim', 2,
        '-stim[0].crct.type', 0,
        '-stim[0].pulse.strength', 200.0,
        '-stim[0].ptcl.duration', 2.0,
        '-stim[0].ptcl.start', 0,
        '-stim[0].elec.geom_type', 2,
        '-stim[0].elec.p0[0]', -10,
        '-stim[0].elec.p0[1]', -10,
        '-stim[0].elec.p0[2]', 0,
        '-stim[0].elec.p1[0]', 10,
        '-stim[0].elec.p1[1]', 51210,
        '-stim[0].elec.p1[2]', 0,
        '-stim[0].ptcl.bcl', 250.0,
        '-stim[0].ptcl.npls', 5,
        '-stim[1].crct.type', 0,
        '-stim[1].pulse.strength', 200.0,
        '-stim[1].ptcl.duration', 2.0,
        '-stim[1].ptcl.start', 1200,
        '-stim[1].elec.geom_type', 2,
        '-stim[1].elec.p0[0]', 0,
        '-stim[1].elec.p0[1]', 0,
        '-stim[1].elec.p0[2]', 0,
        '-stim[1].elec.p1[0]', 25000,
        '-stim[1].elec.p1[1]', 25000,
        '-stim[1].elec.p1[2]', 0
    ]

    cmd += ATs

    if args.eik_sol == 1:
        cmd += [
            '-num_gregions', 1,
            '-gregion[0].num_IDs', 1,
            '-gregion[0].ID[0]', 0,
            '-gregion[0].cv_l', args.velocity,
            '-gregion[0].cv_ar_t', math.sqrt(args.ar),
            '-eik_sol', args.eik_sol,
            '-eik_CVrest_type', 0,
            '-gregion[0].k_cvrest', k_cvrest,
            '-gregion[0].p_cvrest', p_cvrest,
            '-gregion[0].j_cvrest', j_cvrest
        ]
    else:
        gil, gel = execute(args, job, resolution=args.resol, converge=True, lumping=None, ts=None, model=imp_model, modelpar=modelpar, cvfile='results.dat', velocity=args.velocity)
        cmd += [
            '-num_gregions', 1,
            '-gregion[0].num_IDs', 1,
            '-gregion[0].ID[0]', 1,
            '-gregion[0].g_il', gil,
            '-gregion[0].g_it', gil / args.ar,
            '-gregion[0].g_el', gel,
            '-gregion[0].g_et', gel / args.ar
        ]

    cmd_prepace = cmd + stim + tissueinit

    model = {1: 'C', 2: 'B', 3: 'M'}.get(args.model, 'C')
    status = 'A' if args.af_status == 1 else 'H'
    geom = 'S' if args.geom == 0 else 'R'
    dream = 'M' if args.eik_sol == 0 else 'D'
    res = {200: '02', 400: '04', 800: '08', 1600: '16'}.get(args.resol, '04')

    filename = f'Computertimes_{model}{status}{geom}{res}{dream}'
    job_ID_pre = os.path.join(current_directory, 'PrepaceFiles', filename)

    cmd_prepace += [
        '-simID', job_ID_pre,
        '-imp_region[0].im', imp_model,
        '-meshname', meshname,
        '-num_tsav', 1,
        '-tsav[0]', 1400,
        '-tend', 1400.1,
        '-imp_region[0].im_param', modelpar
    ]
    cmd_prepace += ['-gridout_i', 3]

    job.carp(cmd_prepace) #run the prepacing simulation and s1 s2 protocol

    prepace_folder = os.path.join(current_directory, 'PrepaceFiles', filename)
    startstatef = os.path.join(prepace_folder, 'state.1400.0')

    cmd += [
        '-simID', job.ID,
        '-imp_region[0].im', imp_model,
        '-start_statef', startstatef,
        '-meshname', meshname,
        '-tend', 1700,
        '-imp_region[0].im_param', modelpar
    ]
    cmd += ['-gridout_i', 3]

    start = time.perf_counter()
    job.carp(cmd) # Read the saved file and run another 300 ms
    end = time.perf_counter()
    simulation_time = end - start
    print(f'Simulation took {simulation_time} seconds')

    with open(os.path.join(job.ID, 'Time_eikonal.dat'), 'a') as f:
        f.write(f'{simulation_time}\n')

    if args.visualize and not settings.platform.BATCH:
        # Prepare file paths
        geom = meshname
        # display transmembrane voltage
        data = os.path.join(job.ID, 'vm.igb')
        # Call meshalyzer
        job.meshalyzer(geom, data)

if __name__ == '__main__':
    prepace()
